package com.moviles1.helloworld;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.moviles1.helloworld.adapters.UserAdapter;
import com.moviles1.helloworld.database.DbConnection;
import com.moviles1.helloworld.databinding.ActivityListUsersBinding;
import com.moviles1.helloworld.entities.UserEntity;

import java.util.ArrayList;

public class ListUsersActivity extends AppCompatActivity {

    private ActivityListUsersBinding listUsersBinding;
    private DbConnection dbConnection;
    private ArrayList<UserEntity> users;
    private UserAdapter userAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        listUsersBinding = ActivityListUsersBinding.inflate(getLayoutInflater());
        View v = listUsersBinding.getRoot();
        setContentView(v);
        dbConnection = new DbConnection(this);
        users = new ArrayList<>();
        userAdapter = new UserAdapter(users, this);
        listUsersBinding.rvListUsers.setHasFixedSize(true);
        listUsersBinding.rvListUsers.setLayoutManager(new LinearLayoutManager(this));
        listUsersBinding.rvListUsers.setAdapter(userAdapter);
        listUsers();
    }

    public void listUsers(){
        SQLiteDatabase db = dbConnection.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM users", null);
        if(cursor.getCount() > 0){
            while(cursor.moveToNext()){
                int id = Integer.parseInt(cursor.getString(0));
                String name = cursor.getString(1);
                String email = cursor.getString(2);
                int identification = Integer.parseInt(cursor.getString(3));
                String password = cursor.getString(4);
                String role = cursor.getString(5);
                String shop = cursor.getString(6);
                UserEntity user = new UserEntity();
                user.setId(id);
                user.setName(name);
                user.setEmail(email);
                user.setIdentification(identification);
                user.setPassoword(password);
                user.setShop(shop);
                user.setRole(role);
                users.add(user);
            }
            userAdapter.notifyDataSetChanged();
        }
        else{
            Toast.makeText(this, "No hay datos en la db", Toast.LENGTH_SHORT).show();
        }
    }
}
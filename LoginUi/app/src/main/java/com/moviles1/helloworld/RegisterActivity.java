package com.moviles1.helloworld;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Toast;

import com.moviles1.helloworld.database.DbConnection;
import com.moviles1.helloworld.databinding.ActivityRegisterBinding;

public class RegisterActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private DbConnection dbConnection;
    private ActivityRegisterBinding registerBinding;
    private String userRole;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        registerBinding = ActivityRegisterBinding.inflate(getLayoutInflater());
        View v = registerBinding.getRoot();
        setContentView(v);
        registerBinding.spRoles.setOnItemSelectedListener(this);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.roles, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        registerBinding.spRoles.setAdapter(adapter);
        dbConnection = new DbConnection(this);

    }

    public void register(View view){
        SQLiteDatabase db = dbConnection.getWritableDatabase();
        ContentValues userData = new ContentValues();
        String username = registerBinding.etUsername.getText().toString();
        String identification = registerBinding.etIdentification.getText().toString();
        String email = registerBinding.etEmail.getText().toString();
        String password = registerBinding.etPassword.getText().toString();
        userData.put("name",username);
        userData.put("identification", Integer.parseInt(identification));
        userData.put("email", email);
        userData.put("password", password);
        userData.put("role", userRole);
        if(userRole.equals("Vendedor")){
            String shopName = registerBinding.etShopName.getText().toString();
            userData.put("shop", shopName);
        }
        long newUser = db.insert("users", null, userData);
        Toast.makeText(this, "" + newUser, Toast.LENGTH_SHORT).show();
        clearFields();
        Intent intent = new Intent(this, ListUsersActivity.class);
        startActivity(intent);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        userRole = parent.getItemAtPosition(position).toString();
        Toast.makeText(this, ""+userRole, Toast.LENGTH_SHORT).show();
        if(userRole.equals("Vendedor")){
            registerBinding.etShopName.setVisibility(View.VISIBLE);
        }
        else{
            registerBinding.etShopName.setVisibility(View.GONE);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public void goToListUsers(View view){
        Intent intent = new Intent(this, ListUsersActivity.class);
        startActivity(intent);
    }

    public void clearFields(){
        registerBinding.etUsername.setText("");
        registerBinding.etEmail.setText("");
        registerBinding.etIdentification.setText("");
        registerBinding.etPassword.setText("");
    }
}
package com.moviles1.helloworld;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Toast;

import com.moviles1.helloworld.entities.UserEntity;

public class EditUserActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_user);
        UserEntity user = (UserEntity) getIntent().getSerializableExtra("usuario");
        Toast.makeText(this, ""+ user.getId() + ": " + user.getName(), Toast.LENGTH_SHORT).show();
    }
}